# docker_X_example

Ah!  Well I'm sure other people can search, but this is where I stole this.

http://fabiorehm.com/blog/2014/09/11/running-gui-apps-with-docker/

## Why?

This shows an example of how to run a docker, that can natively display X applications on your desktop, without an entire X install/overhead.

The basic premis is that you can share out your x socket with a volume mount. That way you don't need an entire X running in your docker.

I'm sure this has some security ramifications.  But it's still neat.

Similarly, you could share your existing music collection.  In this  case, I run a clean google-music manager so I can upload music.
Note: 

-google music will bail, because google snoops like a mother fucker.  For this reason, the easiest workaround to "verify your computer" is to run with --host.  I don't recommend running dockers this way. But if you want to limit what google music views on your files system, this is still helpful. 

-If you just want to screw around with chrome (or firefox) or google music, or install some other X application, add the the entrypoint override and have at it.

-shm-size needs to be large enough for chromium not to bail. Feel free to decrease it, but if you have stability issues, increase it.

-finally, chromium and firefox expect to have advanced access to your video hardware.  There are helper scripts to launch with those disabled.

## run.sh 

use this to launch your docker.  And entrypoint override (bash) and --net is commented out and there for convenience

    docker run -ti --rm \
           -e DISPLAY=$DISPLAY \
           --shm-size 5G \
           -v /tmp/.X11-unix:/tmp/.X11-unix \
           $1 

           #--entrypoint "/bin/bash" \
           #--net=host \


-run_chrome  a helper to launch chrome within the running docker.  Found in ~/

-run_firefox  a helper to launch firefox (although not needed and likely just wrong)  within the running docker.  Found in ~/

## example

    git clone git@gitlab.com:hibaw/docker_x_example.git
    cd docker_x_example
    docker build -t somename .
    ./run.sh somename

    # cd ~
    # ./run_chrome
