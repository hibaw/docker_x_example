FROM debian:latest

RUN apt-get update && apt-get install -y xdg-utils libqt5webkit5 net-tools qt5-default chromium firefox-esr sudo

#create a user matching id1000 (this should match the running X user) and add them to sudoers
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    chown ${uid}:${gid} -R /home/developer && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer

RUN apt-get install -y wget

#ADD ./google-musicmanager-beta_current_amd64.deb /app/google-musicmanager-beta_current_amd64.deb
#Adding some convenience scripts
ADD ./run_chrome /home/developer/run_chrome
ADD ./run_firefox /home/developer/run_firefox

RUN wget https://dl.google.com/linux/direct/google-musicmanager-beta_current_amd64.deb
RUN dpkg -i ./google-musicmanager-beta_current_amd64.deb

USER developer
ENV HOME /home/developer
ENTRYPOINT ["/app/run_chrome"]



